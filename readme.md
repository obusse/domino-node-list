# Domino V10 App Dev Pack Demo

![Screenshot](screen.png)

This demo utilizes the Domino App Dev Pack for Node.js.

## Setup

On the machine you want to run this demo you need Node.js installed. Grab your version (8+) from their website [https://nodejs.org/en/download/](https://nodejs.org/en/download/).

### Install general packages

This is for your convinience only.

```plaintext
npm install -g nodemon pm2
```

## Clone this repo

Clone this repo to your machine. Enter the root folder and create a new folder called `vendor`

```plaintext
mkdir vendor
```

Copy the `domino-domino-db-x.x.x.tgz` file from the App Dev Pack to this folder.

Run the installer

```plaintext
npm install
```

This will install packages needed for the app.

## NSF source

The app assumes a NSF on a server which hostname is `domino-v10`. You can modify this name in the file `app.js` to your needs.
The NSF file is a blog template I created so you won't have it that way. 

Just create your own NSF and create some documents with this structre:

### Blogpost

- form = "post"
- postTitle as a text field
- postDate as a date field
- postTags as multivalue text field (should contain one or more tag values, see tag document)
- postBody as richtext (not supported in beta 1)

### Tag

- form = "tag"
- tag as text field

You won't need any special views as the app uses DQL only.

## Run the app

In the root folder simply start the app with

```plaintext
nodemon
```

or any other way you prefer like `npm start`.