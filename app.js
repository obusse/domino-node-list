/**
 * demo of node js and domino
 */

var express = require('express');
var app = express();
var domino = require('./functions.js');

// domino-db
const {
    useServer
} = require('@domino/domino-db');

// proton config
const serverConfig = {
    "hostName": "domino10.local",
    "connection": {
        "port": 3002
    }
}

// domino nsf
const databaseConfig = {
    "filePath": "hp.nsf"
};

// templating engine
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// queries
const query1 = "form = 'post' and postDate > @dt('2018-01-01')";
const query2 = "form = 'post' and postTags in ('wtf', 'ibm', 'dql') and postDate > @dt('2018-01-01')";
const query3 = "form = 'post'";
const query = query3;

// cannot be > 200
const maxCount = 200;

// get all posts by tag
function getPostsByTag(tag, callback, errorCallback) {
    useServer(serverConfig).then(async server => {
        const queryTags = "form = 'post' and postTags in ('" + tag + "')";
        const database = await server.useDatabase(databaseConfig);
        const allPosts = await domino.getAllPosts(database, queryTags, maxCount);
        callback(allPosts, queryTags);
    }).catch(error => {
        errorCallback(error);
    });
}

// get all posts from the query
function getAllPosts(callback, errorCallback) {
    useServer(serverConfig).then(async server => {
        const database = await server.useDatabase(databaseConfig);
        const allPosts = await domino.getAllPosts(database, query, maxCount);
        callback(allPosts);
        // console.log(allPosts);
    }).catch(error => {
        errorCallback(error);
    });
}

// load a single post
function getPost(unid, callback, errorCallback) {
    useServer(serverConfig).then(async server => {
        const database = await server.useDatabase(databaseConfig);
        const post = await domino.getPost(database, unid);
        callback(post);
    }).catch(error => {
        errorCallback(error);
    });
}

// ROUTES
// homepage
app.get('/', function (req, res) {
    getAllPosts(function (posts) {
        // console.log(server);
        res.render('index', {
            title: 'Blogposts',
            server: serverConfig.hostName,
            filepath: databaseConfig.filePath,
            message: query,
            documents: posts.documents,
            tags: posts.tags,
            explain: posts.explain,
            count: posts.documentRange.count,
            total: posts.documentRange.total,
            max: maxCount
        });
    }, function (error) {
        renderErrorPage(res, error);
    });
});

// posts by tag
app.get('/bytag', function (req, res) {
    getPostsByTag(req.query.tag, function (posts, queryTag) {
        // console.log(server);
        res.render('index', {
            title: 'Blogposts by tag',
            server: serverConfig.hostName,
            filepath: databaseConfig.filePath,
            message: queryTag,
            documents: posts.documents,
            tags: posts.tags,
            explain: posts.explain,
            count: posts.documentRange.count,
            total: posts.documentRange.total,
            max: maxCount
        });
    }, function (error) {
        renderErrorPage(res, error);
    });

});

// blog post
app.get('/post', function (req, res) {
    // console.log("unid", req.query.id);
    getPost(req.query.id, function (post) {
        res.render('post', {
            title: 'Blogpost',
            postId: post.postId,
            postTitle: post.postTitle,
            postDate: post.postDate.data,
            postTags: post.postTags,
            postBody: post.postBody
        });
    }, function (error) {
        renderErrorPage(res, error);
    });
});

// error page
renderErrorPage = function (res, error) {
    res.render('error', {
        title: "Oops, something went wrong!",
        message: error.message,
        code: error.code
    })
}

// about page
app.get('/about', function (req, res) {
    res.render('about', {
        title: 'About this app'
    })
});

// start server
var server = app.listen(3000, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})