module.exports.getAllPosts = async function(database, query, count) {
    try {
        // find the posts
        const docs = await database.bulkReadDocuments({
            query: query,
            count: count,
            onErrorOptions: "ON_ERROR_CONTINUE",
            itemNames: ['postId', 'postTitle', 'postDate', 'postBody']
        });

        // find all tags
        const tags = await database.bulkReadDocuments({
            query: "form = 'tag'",
            count: 200,
            onErrorOptions: "ON_ERROR_CONTINUE",
            itemNames: ['tag']
        });

        // unique the tags
        var lookup = {};
        var items = tags.documents;
        var tagsSorted = [];

        for (var item, i = 0; item = items[i++];) {
            var tag = item.tag;

            if (!(tag in lookup)) {
                lookup[tag] = 1;
                tagsSorted.push(tag);
            }
        }

        // get explanation
        const explain = await database.explainQuery({
            query: query
        });

        // sort posts
        docs.documents = docs.documents.sort(function(a, b) {
            return new Date(b.postDate.data).getTime() - new Date(a.postDate.data).getTime();
        });

        // sort tags
        docs.tags = tagsSorted.sort(function(a, b) {
            var tagA = a.toUpperCase(); // ignore upper and lowercase
            var tagB = b.toUpperCase(); // ignore upper and lowercase
            if (tagA < tagB) {
                return -1;
            }
            if (tagA > tagB) {
                return 1;
            }

            // names must be equal
            return 0;
        });

        // add explanatin
        docs.explain = explain;
        return docs;
    } catch (e) {
        console.log("getDocs:", e);
        throw e;
    }
};

module.exports.getPost = async function(database, unid) {
    try {
        const doc = await database.useDocument({
            unid: unid
        });
        const post = await doc.read({
            itemNames: ["postId", "postTitle", "postTags", "postDate", "postBody"]
        })
        console.log("post", post);
        return post;
    } catch (e) {
        throw e;
    }
}